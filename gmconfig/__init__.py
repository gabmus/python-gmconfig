name = 'gmconfig'

import json
from os.path import isfile, isdir
from os import makedirs
from pathlib import Path

class GMConfig:

    def save(self, n_config=None):
        if not n_config:
            n_config = self.config
        if not self.plib_path.parent.is_dir():
            makedirs(str(self.plib_path.parent))
        with open(self.path, 'w') as fd:
            fd.write(json.dumps(n_config))
            fd.close()

    def _load_ret(self):
        '''
        Load (or generates) the configuration file and returns the
        dictionary instead of saving it to self.config.

        This is a "private" method and shall not be used outside
        of this class.
        '''
        c_config = None
        if not isfile(self.path):
            c_config = self.default_schema
            self.save(c_config)
            return c_config
        else:
            with open(self.path, 'r') as fd:
                c_config = json.loads(fd.read())
                fd.close()
            do_save = False
            for key in self.default_schema.keys():
                if key not in c_config.keys():
                    c_config[key] = self.default_schema[key]
                    do_save = True
            if do_save:
                self.save(c_config)
            return c_config


    def __init__(self, path, default_schema):
        if not type(default_schema) == dict:
            raise TypeError(
                'Provided default_schema is {0} and should be dict'.format(
                    type(default_schema)
                )
            )
            return None
        self.path = path
        self.plib_path = Path(path) # pathlib path, necessary for some checks
        self.default_schema = default_schema
        self.config = self._load_ret()

    def _key_exists(self, key):
        '''
        Check if a key exists inside the config.

        If exists returns True, else throws an exception and
        returns False
        '''
        if key not in self.config.keys():
            raise KeyError(
                'Key {0} not found in the configuration. Try changing default_schema.'.format(
                    key
                )
            )
            return False
        else:
            return True

    def get(self, key):
        if not self._key_exists(key):
            return None
        else:
            return self.config[key]

    def set(self, key, value):
        if not self._key_exists(key):
            return
        else:
            self.config[key] = value
            self.save()

