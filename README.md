# GMConfig

Simple JSON based configuration file manager for python 3.x.

This module has been created mostly for my personal use in my Python projects. Feel free to use it, but if you want to incorporate it in your project, you're better off forking it and adapting it to your needs.

This module is very specifically designed for my personal code style and will probably change in time, breaking compatibility.
