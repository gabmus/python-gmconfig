import setuptools

with open("README.md", "r") as fd:
    long_description = fd.read()

setuptools.setup(
    name="gmconfig",
    version="0.1",
    author="Gabriele Musco",
    author_email="emaildigabry@gmail.com",
    description="Simple JSON based configuration file manager for python 3.x<Paste>",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gabmus/python-gmconfig",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ],
)
